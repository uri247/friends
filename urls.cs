﻿using System;

namespace friends
{
    public class FbUrls
    {
        private const string appid = "194079944003155";
        private const string appsecret = "b243fcdf2fdd7edf4c94ce89c02129de";
        private const string graph = "graph.facebook.com";
        private const string ui = "www.facebook.com";
        private const string scope = "user_online_presence,friends_online_presence";

        static public string authdlg() {
            return string.Format( "https://www.facebook.com/dialog/oauth?client_id={0}&redirect_uri={1}&response_type=token&scope={2}", appid, success(), scope );
        }

        static public string success() {
            return "https://www.facebook.com/connect/login_success.html";
        }

        static public string successPath() {
            return "/connect/login_success.html";
        }

        static public string logout( string accessToken ) {
            return string.Format( "https://{0}/logout.php?next={1}&access_token={2}", ui, success( ), accessToken );
        }

        static public string meUrl( string accessToken ) { 
            return string.Format( "https://{0}/me?access_token={1}", graph, accessToken );
        }

        static public string pictUrl( string id, string type ) { return string.Format( "http://graph.facebook.com/{0}/picture?type={1}", id, type ); }


        static public string friends( string accessToken ) { return string.Format( "https://{0}/me/friends?access_token={1}", graph, accessToken ); }




        static public string authapi( string code, string redirect ) {
            return string.Format( "https://{0}/oauth/access_token?client_id={1}&redirect_uri={2}&client_secret={3}&code={4}",
                graph,
                appid,
                redirect,
                appsecret,
                code
                );
        }

        static public string fql( string q, string accessToken ) {
            string uq = q.Replace( ' ', '+' );
            return string.Format( "https://{0}/fql?q={1}&access_token={2}", graph, uq, accessToken );
        }

        static public string friendsFql( string accessToken ) {
            return fql( "SELECT uid, name, username, online_presence FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me())", accessToken );
        }

    }
}