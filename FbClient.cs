﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace friends
{
    public class FbClient
    {
        private string m_token;
        private string m_expires;

        public FbClient( string token, string expires ) {
            m_token = token;
            m_expires = expires;
        }

        public async Task<FbUser> GetMeAsync( )
        {

            HttpClient client = new HttpClient();
            string respstr = await client.GetStringAsync( FbUrls.meUrl(m_token) );
            FbUser me = JsonConvert.DeserializeObject<FbUser>( respstr );
            return me;
        }

        public async Task<FbFriends> GetFriendsAsync()
        {
            HttpClient client = new HttpClient();
            string respstr = await client.GetStringAsync(FbUrls.friends(m_token));
            FbFriends friends = JsonConvert.DeserializeObject<FbFriends>(respstr);
            return friends;
        }

    }
}
