﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.IO;
using System.Net.Http;
using System.Windows.Media.Imaging;

namespace friends
{

    public class SampleUser
    {
        public string name { get; set; }
        public string id { get; set; }
    };

    public class SampleFriendsList : List<SampleUser>
    {
    };

    public class SampleVM
    {
        public string appName { get; set; }
        public bool loggedIn { get; set; }
        public SampleUser user { get; set; }
        public SampleFriendsList friends { get; set; }
    };


    public class FbUserVM : PropertyBag
    {
        private FbUser m_model;


        public FbUserVM(FbUser model) {
            m_model = model;
        }

        public string name {
            get { return m_model.name; }
        }

        public string smallPicture {
            get { return FbUrls.pictUrl(m_model.id, "small"); }
        }

        public string normalPicture {
            get { return FbUrls.pictUrl(m_model.id, "normal"); }
        }

        public string largePicture
        {
            get { return FbUrls.pictUrl( m_model.id, "large" ); }
        }
    }

    public class FbFriendVM : PropertyBag
    {
        private static string l_location;

        private FbFriend m_model;

        public string id                { get { return m_model.id; } }
        public string name                      { get { return m_model.name; } }        

        static FbFriendVM( )
        {
            string appdata = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );
            string myappdata = Path.Combine( appdata, "friends" );
            if( !Directory.Exists( myappdata ) )
            {
                Directory.CreateDirectory( myappdata );
            }
            l_location = Path.Combine( myappdata, "imgs" );
            if( !Directory.Exists( l_location ) )
            {
                Directory.CreateDirectory( l_location );
            }
        }

        public FbFriendVM( FbFriend model )
        {
            m_model = model;
        }

    }

    class MainVM : PropertyBag
    {
        public event UIRequestEventHandler uireqFacebookLogin;
        public event UIRequestEventHandler uireqFacebookLogout;
        public event UIRequestEventHandler uireqExitApplication;

        public DelegateCommand cmdLogin { get; set; }
        public DelegateCommand cmdLogout { get; set; }
        public DelegateCommand cmdExit { get; set; }
        public DelegateCommand cmdView { get; set; }

        private bool m_loggedIn;
        private string m_fbToken;
        private string m_fbExpires;
        private FbClient m_fbClient;
        private FbUserVM m_user;
        private ObservableCollection<FbFriendVM> m_friends;

        public bool loggedIn {
            get { return m_loggedIn; }
            set { m_loggedIn = value; raiseUser(); }
        }

        public FbUserVM user {
            get { return m_user; }
            set { m_user = value; raiseUser(); }
        }

        private string m_appName;
        public string appName {
            get { return m_appName; }
            set { m_appName = value; raise( "appName" ); }
        }

        public ObservableCollection<FbFriendVM> friends {
            get { return m_friends; }
            set { m_friends = value; raise("friends"); }
        }

        private string m_viewMode;
        public string viewMode {
            get { return m_viewMode; }
            set { m_viewMode = value; raise( "viewMode" ); }
        }

        private void raiseUser() {
            raise("loggedIn");
            raise("mySmallPicture");
            raise("myNormalPicture");
            raise("myName");
            raise("user");
            cmdLogin.raiseChanged();
            cmdLogout.raiseChanged();
        }


        public MainVM( )
        {
            cmdLogin = new DelegateCommand( x => !loggedIn, x => facebookLogin() );
            cmdLogout = new DelegateCommand( x => loggedIn, x => facebookLogout() );
            cmdExit = new DelegateCommand( x => true, x => exit() );
            cmdView = new DelegateCommand( x => true, setView );

            this.loggedIn = false;
            this.viewMode = "small";
        }

        void facebookLogin( ) {
            PopupBrzVM brzvm = new PopupBrzVM {
                title = "Facebook Login", 
                url=FbUrls.authdlg(),
                logic=loginLogic,
                completedDelegate = initUser
            };

            if( uireqFacebookLogin != null ) {
                uireqFacebookLogin( this, new UIRequestEventArgs(brzvm) );
            }
        }

        async void initUser( PopupBrzVM brzvm ) {
            if( brzvm.result == BrzResult.Success ) {
                m_fbClient = new FbClient( m_fbToken, m_fbExpires );
                loggedIn = true;

                FbUser u = await m_fbClient.GetMeAsync();
                this.user = new FbUserVM(u);

                FbFriends fs = await m_fbClient.GetFriendsAsync();
                this.friends = new ObservableCollection<FbFriendVM>();
                foreach( FbFriend f in fs.data ) {
                    this.friends.Add( new FbFriendVM(f) );
                }
                this.user = new FbUserVM(await m_fbClient.GetMeAsync());
            }            
        }


        void facebookLogout( ) {
            PopupBrzVM brzvm = new PopupBrzVM {
                title = "Facebook Logout",
                url = FbUrls.logout(m_fbToken),
                logic = logoutLogic,
                completedDelegate = delegate (PopupBrzVM vm) {
                    loggedIn = false;
                    m_fbClient = null;
                    this.user = null;
                    this.friends = null;

                }
            };

            if( uireqFacebookLogout != null ) {
                uireqFacebookLogout( this, new UIRequestEventArgs(brzvm) );
            }
        }

        BrzAction loginLogic( Uri uri )
        {
            string path = uri.LocalPath;

            if( path == FbUrls.successPath() ) {
                string frag = uri.Fragment;
                int i1 = frag.IndexOf( "access_token" );
                int e1 = frag.IndexOf( '&', i1 + 13 );
                m_fbToken = frag.Substring( i1+13, e1-i1-13 );
                int i2 = frag.IndexOf( "expires_in=", e1 );
                m_fbExpires = frag.Substring( i2 + 11 );
                return BrzAction.Success;
            }
            else {
                return BrzAction.Visible;
            }
        }

        BrzAction logoutLogic( Uri uri )
        {
            string path = uri.LocalPath;
            if( path == FbUrls.successPath() )
            {
                m_fbToken = null;
                m_fbExpires = null;
                return BrzAction.Success;
            }
            else
            {
                return BrzAction.Visible;
            }
        }


        void exit( )
        {
            if( uireqExitApplication != null ) {
                uireqExitApplication( this, null );
            }
        }

        void setView( object o )
        {
            this.viewMode = (string)o;
        }
    }
}
