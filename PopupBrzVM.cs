﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace friends
{
    public enum BrzAction { Success, Fail, Keep, Visible };
    public enum BrzResult { Success, Fail, Cancel };

    public delegate BrzAction BrzLogicDelegate( Uri uri );
    public delegate void BrzCompletedDelegate( PopupBrzVM brzvm );

    public class PopupBrzVM
    {
        public string title { get; set; }
        public string url { get; set; }
        public BrzLogicDelegate logic { get; set; }
        public BrzResult result { get; set; }
        public BrzCompletedDelegate completedDelegate { get; set; }
    }
}
