﻿using System;
using System.Windows.Input;

namespace friends
{
    public class DelegateCommand : ICommand
    {
        private readonly Predicate<object> m_canExecuteDel;
        private readonly Action<object> m_executeDel;
        public event EventHandler CanExecuteChanged;

        public DelegateCommand(Predicate<object> canExecuteDel, Action<object> executeDel)
        {
            m_canExecuteDel = canExecuteDel;
            m_executeDel = executeDel;
        }

        public bool CanExecute(object param)
        {
            return m_canExecuteDel == null ? true : m_canExecuteDel(param);
        }

        public void Execute(object param)
        {
            if (m_executeDel != null)
            {
                m_executeDel(param);
            }
        }

        public void raiseChanged()
        {
            if (CanExecuteChanged != null)
            {
                CanExecuteChanged(this, null);
            }
        }

    }

}
