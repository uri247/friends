﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace friends
{
    /// <summary>
    /// Interaction logic for PopupBrz.xaml
    /// </summary>
    public partial class PopupBrz : Window
    {
        public int fullHeight { get; set; }
        public int fullWidth { get; set; }

        public PopupBrz( PopupBrzVM brzvm, int width, int height ) {
            InitializeComponent( );
            this.DataContext = brzvm;
            fullWidth = width;
            fullHeight = height;
        }

        private void WebBrowser_Loaded( object sender, RoutedEventArgs args ) {
            PopupBrzVM brzvm = (PopupBrzVM)this.DataContext;
            x_brz.LoadCompleted += handlerLoadCompleted;
            x_brz.Navigate( brzvm.url );
        }

        void handlerLoadCompleted( object sender, System.Windows.Navigation.NavigationEventArgs args )
        {
            PopupBrzVM brzvm = (PopupBrzVM)this.DataContext;
            Uri uri = args.Uri;
            BrzAction action = brzvm.logic( uri );

            if( action == BrzAction.Fail ) {
                brzvm.result = BrzResult.Fail;
                this.Close();
            }
            else if( action == BrzAction.Success ) {
                brzvm.result = BrzResult.Success;
                this.Close();
            }
            else if( action == BrzAction.Visible ) {
                this.Width = fullWidth;
                this.Height = fullHeight;
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                this.ShowInTaskbar = true;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            PopupBrzVM brzvm = (PopupBrzVM)this.DataContext;
            if (brzvm.completedDelegate != null) {
                brzvm.completedDelegate(brzvm);
            }
        }

    }
}
