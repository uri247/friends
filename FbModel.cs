﻿using System;
using System.Collections.Generic;

namespace friends
{
    public class FbUser
    {
        public string id { get; set; }
        public string name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string link { get; set; }
        public int timezone { get; set; }
        public string locale { get; set; }
        public bool verified { get; set; }
        public DateTime updated_time { get; set; }
    };

    public class FbFriend
    {
        public string id { get; set; }
        public string name { get; set; }
    };

    public class FbFriends
    {
        public List<FbFriend> data { get; set; }
    };

}
