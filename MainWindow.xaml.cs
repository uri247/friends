﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace friends
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow( )
        {
            InitializeComponent();

            MainVM vm = (MainVM)this.DataContext;
            vm.uireqFacebookLogin += handlerFb;
            vm.uireqFacebookLogout += handlerFb;
            vm.uireqExitApplication += ( sender, args ) => this.Close();
            vm.PropertyChanged += propertyChanged;
        }

        void handlerFb( object sender, UIRequestEventArgs args )
        {
            var dc = (PopupBrzVM)args.dataContext;
            PopupBrz brz = new PopupBrz( dc, 1024, 603 );
            brz.Show();
        }

        void propertyChanged( object sender, PropertyChangedEventArgs e )
        {
            MainVM vm = (MainVM)this.DataContext;
            if( e.PropertyName == "viewMode" )
            {
                ControlTemplate tmpl = this.FindResource( vm.viewMode ) as ControlTemplate;
                x_list.Template = tmpl;
            }
        }

    }

}