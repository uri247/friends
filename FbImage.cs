﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.IO;

namespace friends
{
    public class FbImage : Image
    {
        public static readonly DependencyProperty IdProperty = DependencyProperty.Register(
            "fbid", typeof( string ), typeof( FbImage ), new PropertyMetadata( "", idChanged_ ) );
        public static readonly DependencyProperty SizeProperty = DependencyProperty.Register(
            "fbsize", typeof( string ), typeof( FbImage ), new PropertyMetadata( "", sizeChanged_ ) );

        public static readonly string location = @"C:\Users\uri\AppData\Local\friends\imgs";

        private bool m_fbidSet = false;
        private bool m_fbsizeSet = false;

        static FbImage( )
        {
        }


        public FbImage( )
            : base()
        {
        }


        static void idChanged_( DependencyObject d, DependencyPropertyChangedEventArgs e )
        {
            FbImage fbimg = d as FbImage;
            fbimg.m_fbidSet = true;
            //string newFbid = e.NewValue as string;
            fbimg.updateImage();
        }

        static void sizeChanged_( DependencyObject d, DependencyPropertyChangedEventArgs e )
        {
            FbImage fbimg = d as FbImage;
            fbimg.m_fbsizeSet = true;
            fbimg.updateImage();
        }

        void updateImage( )
        {
            if( m_fbidSet && m_fbsizeSet )
            {
                if( File.Exists( this.localPath ) )
                {
                    this.Source = new BitmapImage( new Uri( this.localPath ) );
                }
                else
                {
                    BitmapImage img = new BitmapImage( new Uri( imageUrl ) );
                    img.DownloadCompleted += img_DownloadCompleted;
                    this.Source = img;
                }
            }
        }

        void img_DownloadCompleted( object sender, EventArgs e )
        {
            BitmapImage img = (BitmapImage)sender;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();

            BitmapFrame frame = BitmapFrame.Create( img );
            encoder.Frames.Add( frame );
            using( FileStream strm = new FileStream( this.localPath, FileMode.Create ) )
            {
                encoder.Save( strm );
            }
        }

        public string fbid
        {
            get { return base.GetValue( IdProperty ) as string; }
            set { base.SetValue( IdProperty, value ); }
        }

        public string fbsize
        {
            get { return base.GetValue( SizeProperty ) as string; }
            set { base.SetValue( SizeProperty, value ); }
        }

        public string localPath
        {
            get { return Path.Combine( location, fbid + "_" + fbsize + ".jpg" ); }
        }

        public string imageUrl
        {
            get { return string.Format( "http://graph.facebook.com/{0}/picture?type={1}", fbid, fbsize ); }
        }


    }
}
