﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace friends
{
    public class UIRequestEventArgs
    {
        public object dataContext { get; set; }
        public UIRequestEventArgs( object dc ) {
            dataContext = dc;
        }
    }

    public delegate void UIRequestEventHandler( object sender, UIRequestEventArgs args );
}
